/**
 * 2014
 * 18 mai 2014 15:27:18 
 * @author stephane stephane.gronowski@gmail.com
 */
package com.svan.javacard.cardUtils.request.sle5528;

import java.math.BigInteger;

import com.svan.javacard.cardUtils.request.common.AbstractRequest;
import com.svan.javacard.cardUtils.request.common.WriteRequest;

/**
 * 
 * 18 mai 2014 15:27:18
 * 
 * @author stephane stephane.gronowski@gmail.com
 * 
 */
public class Write5528 extends AbstractRequest implements WriteRequest{

	public static final Integer MAX_LENGTH = Integer.valueOf(255);
	public static final Integer MAX_OFFSET = Integer.valueOf(65535);
	public static final byte INSTRUCTIONS = ((byte) 0xD0);

	/**
	 * @param cla
	 * @param ins
	 */
	public Write5528() {
		super(DEFAULT_CLA, INSTRUCTIONS);
	}

	private void setLength(Integer length) {
		if (length >= MAX_LENGTH) {
			throw new IllegalArgumentException("Max length is " + MAX_LENGTH + ", cannot set length to " + length);
		}

		setNe(length);
	}

	public void setOffset(Integer offset) {
		if (offset >= MAX_OFFSET) {
			throw new IllegalArgumentException("Max offset is " + MAX_OFFSET + ", cannot set offset to " + offset);
		}

		byte[] lengthByteArray = BigInteger.valueOf(offset).toByteArray();

		// Most Significant Bit
		setP1(lengthByteArray[0]);
		// Less Significant Bit
		if (lengthByteArray.length > 1) {
			setP2(lengthByteArray[1]);
		}

	}
	
	@Override
	public void setData(byte[] data) {
		super.setData(data);
		
		setLength(data.length);
	}
}
