/**
 * 2014
 * 18 mai 2014 15:27:18 
 * @author stephane stephane.gronowski@gmail.com
 */
package com.svan.javacard.cardUtils.request.sle5528;

import com.svan.javacard.cardUtils.request.common.AbstractRequest;

/**
 * RETURN :
 * <ul>
 * <li>FF => OK</li>
 * <li>00 => locked</li>
 * <li>other => other</li>
 * </ul>
 * Plus 2 dummies byte
 * 90 00 si pas d'erreur
 * 
 * 18 mai 2014 15:27:18
 * 
 * @author stephane stephane.gronowski@gmail.com
 * 
 */
public class ReadErrorCpt5528 extends AbstractRequest {

	public static final byte INSTRUCTIONS = ((byte) 0xB1);

	/**
	 * @param cla
	 * @param ins
	 */
	public ReadErrorCpt5528() {
		super(DEFAULT_CLA, INSTRUCTIONS);

		setNe(3);
	}

}
