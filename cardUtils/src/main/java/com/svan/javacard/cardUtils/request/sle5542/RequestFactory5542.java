/**
 * 2014
 * 15 juin 2014 15:41:12 
 * @author stephane stephane.gronowski@gmail.com
 */
package com.svan.javacard.cardUtils.request.sle5542;

import com.svan.javacard.cardUtils.request.common.Instruction;
import com.svan.javacard.cardUtils.request.common.ReadRequest;
import com.svan.javacard.cardUtils.request.common.Request;
import com.svan.javacard.cardUtils.request.common.RequestFactory;
import com.svan.javacard.cardUtils.request.common.VerifPSCRequest;
import com.svan.javacard.cardUtils.request.common.WriteRequest;
import com.svan.javacard.cardUtils.request.sle5528.Read5528;
import com.svan.javacard.cardUtils.request.sle5528.Write5528;

/**
 * 
 * 15 juin 2014 15:41:12
 * 
 * @author stephane stephane.gronowski@gmail.com
 * 
 */
public class RequestFactory5542 implements RequestFactory {

	public Request getRequest(Instruction instruction) {
		switch (instruction) {
		case READ_ERROR_COMPTEUR:
			return new ReadErrorCpt5542();
		default:
			throw new IllegalArgumentException(
					"Cannot build this request (either doesn't exists or cannot be instanciated without more parameters (=>"
							+ instruction);
		}
	}

	@Override
	public ReadRequest getReadRequest() {
		// ISO 5528
		return new Read5528();
	}

	@Override
	public ReadRequest getReadRequest(Integer length) {
		ReadRequest request = getReadRequest();

		request.setLength(length);

		return request;
	}

	@Override
	public ReadRequest getReadRequest(Integer length, Integer offset) {
		ReadRequest request = getReadRequest();

		request.setLength(length);
		request.setOffset(offset);

		return request;
	}

	@Override
	public WriteRequest getWriteRequest() {
		// ISO 5528
		return new Write5528();
	}

	@Override
	public WriteRequest getWriteRequest(Integer offset) {
		WriteRequest request = getWriteRequest();

		request.setOffset(offset);

		return request;
	}

	@Override
	public WriteRequest getWriteRequest(Integer offset, byte[] data) {
		WriteRequest request = getWriteRequest();

		request.setOffset(offset);
		request.setData(data);

		return request;
	}

	@Override
	public VerifPSCRequest getVerifPSCRequest() {
		return new VerifPSC5542();
	}

	@Override
	public VerifPSCRequest getVerifPSCRequest(Integer code) {
		VerifPSCRequest request = getVerifPSCRequest();

		request.setCode(code);

		return request;
	}

}
