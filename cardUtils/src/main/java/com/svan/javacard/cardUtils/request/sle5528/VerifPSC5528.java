/**
 * 2014
 * 18 mai 2014 15:27:18 
 * @author stephane stephane.gronowski@gmail.com
 */
package com.svan.javacard.cardUtils.request.sle5528;

import java.nio.ByteBuffer;

import com.svan.javacard.cardUtils.request.common.AbstractRequest;
import com.svan.javacard.cardUtils.request.common.VerifPSCRequest;

/**
 * Return => 
 * 90 FF => OK
 * 90 00 => locked
 * 90 XX => failed
 * 18 mai 2014 15:27:18
 * 
 * @author stephane stephane.gronowski@gmail.com
 * 
 */
public class VerifPSC5528 extends AbstractRequest  implements VerifPSCRequest {
	
	public static final byte INSTRUCTIONS = ((byte) 0x20);
	
	public VerifPSC5528() {
		super(DEFAULT_CLA, INSTRUCTIONS);
		// taille du code
		setNe((byte) 0x02);
	}

	public void setCode(Integer code) {
		ByteBuffer buf = ByteBuffer.allocate(2);
		// b.order(ByteOrder.BIG_ENDIAN);
		// optional, the initial order of a byte buffer is always BIG_ENDIAN.
		buf.putShort(code.shortValue());

		setData(buf.array());
	}
}
