/**
 * 2014
 * 18 mai 2014 15:27:18 
 * @author stephane stephane.gronowski@gmail.com
 */
package com.svan.javacard.cardUtils.request.sle5528;

import java.math.BigInteger;

import com.svan.javacard.cardUtils.request.common.AbstractRequest;
import com.svan.javacard.cardUtils.request.common.ReadRequest;

/**
 * 
 * 18 mai 2014 15:27:18
 * 
 * @author stephane stephane.gronowski@gmail.com
 * 
 */
public class Read5528 extends AbstractRequest implements ReadRequest{

	/*
	 * So, to write a simple program which first reads 14 bytes from address A4
	 * 03. then verify with the card using the default PSC code. and at last
	 * changes the PSC code to A2 B2; we will do this:
	 * 
	 * << FF B2 A4 03 0E
		>> FF FF FF FF FF FF FF FF FF FF FF FF FF FF 90 00
		<< FF 20 00 00 02 FF FF
		>> FF FF FF 90 00
		<< FF D0 03 FD 03 FF A2 B2
		>> FF A2 B2 90 00
	 */

	public static final Integer MAX_LENGTH = Integer.valueOf(255);
	public static final Integer MAX_OFFSET = Integer.valueOf(65535);
	public static final byte INSTRUCTIONS = ((byte) 0xB0);

	/**
	 * @param cla
	 * @param ins
	 */
	public Read5528() {
		super(DEFAULT_CLA, INSTRUCTIONS);
	}

	public void setLength(Integer length) {
		if (length >= MAX_LENGTH) {
			throw new IllegalArgumentException("Max length is " + MAX_LENGTH + ", cannot set length to " + length);
		}

		setNe(length);
	}

	public void setOffset(Integer offset) {
		if (offset >= MAX_OFFSET) {
			throw new IllegalArgumentException("Max offset is " + MAX_OFFSET + ", cannot set offset to " + offset);
		}

		byte[] lengthByteArray = BigInteger.valueOf(offset).toByteArray();

		// Most Significant Bit
		setP1(lengthByteArray[0]);
		// Less Significant Bit
		if (lengthByteArray.length > 1) {
			setP2(lengthByteArray[1]);
		}
	}
}
