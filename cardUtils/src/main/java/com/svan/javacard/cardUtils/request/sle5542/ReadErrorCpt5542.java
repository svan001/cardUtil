/**
 * 2014
 * 18 mai 2014 15:27:18 
 * @author stephane stephane.gronowski@gmail.com
 */
package com.svan.javacard.cardUtils.request.sle5542;

import com.svan.javacard.cardUtils.request.common.AbstractRequest;

/**
 * RETURN :
 * <ul>
 * <li>07 => OK</li>
 * <li>00 => locked</li>
 * <li>other => other</li>
 * </ul>
 * Plus 3 dummies byte
 * 90 00 si pas d'erreur
 * 
 * 18 mai 2014 15:27:18
 * 
 * @author stephane stephane.gronowski@gmail.com
 * 
 */
public class ReadErrorCpt5542 extends AbstractRequest {

	public static final byte INSTRUCTIONS = ((byte) 0xB1);

	/**
	 * @param cla
	 * @param ins
	 */
	public ReadErrorCpt5542() {
		super(DEFAULT_CLA, INSTRUCTIONS);

		setNe(4);
	}

}
