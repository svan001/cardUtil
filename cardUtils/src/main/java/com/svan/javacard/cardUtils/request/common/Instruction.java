/**
 * 2014
 * 3 mai 2014 00:48:54 
 * @author stephane stephane.gronowski@gmail.com
 */
package com.svan.javacard.cardUtils.request.common;

/**
 * 
 * 3 mai 2014 00:48:54
 * 
 * @author stephane stephane.gronowski@gmail.com
 * 
 */
public enum Instruction {
	// http://stackoverflow.com/questions/6895072/scr3310v2-0-and-sle5528-read-write
	/** Read the memory */
	READ,
	/** Write in the memory */
	WRITE,
	/** Enter PIN code to be authorized to write */
	ENTER_PIN,
	/** Check the stat of the card */
	READ_ERROR_COMPTEUR;

}
