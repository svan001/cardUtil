/**
 * 2014
 * 15 juin 2014 15:42:15 
 * @author stephane stephane.gronowski@gmail.com
 */
package com.svan.javacard.cardUtils.request.common;

import javax.smartcardio.CommandAPDU;

/**
 * 
 * 15 juin 2014 15:42:15
 * 
 * @author stephane stephane.gronowski@gmail.com
 * 
 */
public interface Request {

	/**
	 * 
	 * @return un nouveau {@link CommandAPDU}
	 */
	public CommandAPDU getCommandAPDU();
	
	public byte getCla();

	public void setCla(byte cla);

	public byte getIns();

	public void setIns(byte ins);

	public byte getP1();

	public void setP1(byte p1);

	public byte getP2();

	public void setP2(byte p2);

	public byte[] getData();

	public void setData(byte[] data);

	public int getNe();

	public void setNe(int ne);

}
