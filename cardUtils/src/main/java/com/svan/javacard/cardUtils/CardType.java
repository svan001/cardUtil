/**
 * 2014
 * 15 juin 2014 15:58:34 
 * @author stephane stephane.gronowski@gmail.com
 */
package com.svan.javacard.cardUtils;

/**
 * 
 * 15 juin 2014 15:58:34
 * 
 * @author stephane stephane.gronowski@gmail.com
 * 
 */
public enum CardType {

	SLE_55_42, SLE_55_28;

}
