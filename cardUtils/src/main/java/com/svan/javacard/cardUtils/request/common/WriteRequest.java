/**
 * 2014
 * 15 juin 2014 16:15:41 
 * @author stephane stephane.gronowski@gmail.com
 */
package com.svan.javacard.cardUtils.request.common;

/**
 * 
 * 15 juin 2014 16:15:41
 * 
 * @author stephane stephane.gronowski@gmail.com
 * 
 */
public interface WriteRequest extends Request {

	public void setOffset(Integer offset);

}
