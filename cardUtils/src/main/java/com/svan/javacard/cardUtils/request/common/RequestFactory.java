/**
 * 2014
 * 15 juin 2014 16:31:16 
 * @author stephane stephane.gronowski@gmail.com
 */
package com.svan.javacard.cardUtils.request.common;


/**
 * 
 * 15 juin 2014 16:31:16
 * 
 * @author stephane stephane.gronowski@gmail.com
 * 
 */
public interface RequestFactory {
	public Request getRequest(Instruction instruction);

	public ReadRequest getReadRequest();

	public ReadRequest getReadRequest(Integer length);

	public ReadRequest getReadRequest(Integer length, Integer offset);

	public WriteRequest getWriteRequest();

	public WriteRequest getWriteRequest(Integer offset);

	public WriteRequest getWriteRequest(Integer offset, byte[] data);

	public VerifPSCRequest getVerifPSCRequest();

	public VerifPSCRequest getVerifPSCRequest(Integer code);

}
