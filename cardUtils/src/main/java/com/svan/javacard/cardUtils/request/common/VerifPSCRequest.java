/**
 * 2014
 * 15 juin 2014 16:27:46 
 * @author stephane stephane.gronowski@gmail.com
 */
package com.svan.javacard.cardUtils.request.common;

/**
 * 
 * 15 juin 2014 16:27:46
 * 
 * @author stephane stephane.gronowski@gmail.com
 * 
 */
public interface VerifPSCRequest extends Request {
	public void setCode(Integer code);
}
