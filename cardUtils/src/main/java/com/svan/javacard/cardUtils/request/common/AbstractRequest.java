/**
 * 2014
 * 18 mai 2014 14:49:15 
 * @author stephane stephane.gronowski@gmail.com
 */
package com.svan.javacard.cardUtils.request.common;

import javax.smartcardio.CommandAPDU;


/**
 * 
 * 18 mai 2014 14:49:15
 * 
 * @author stephane stephane.gronowski@gmail.com
 * 
 */
public abstract class AbstractRequest implements Request {

	public static final Byte DEFAULT_CLA = (byte) 0xFF;

	// appli
	private byte cla;

	// instruction
	private byte ins;

	// param 1
	private byte p1;

	// param 2
	private byte p2;

	// taille a lire ??
	private int ne;

	// donnee
	private byte[] data;

	public AbstractRequest(byte cla, byte ins) {
		this.cla = cla;
		this.ins = ins;
	}

	/**
	 * 
	 * @return un nouveau {@link CommandAPDU}
	 */
	public CommandAPDU getCommandAPDU() {
		if (data != null) {
			return new CommandAPDU(cla, ins, p1, p2, data, ne);
		} else {
			return new CommandAPDU(cla, ins, p1, p2, ne);
		}

	}

	/**
	 * Pour affichage des donnes pour humain
	 * 
	 * @param bytes
	 * @return
	 */
	public static String serializeInstructions(byte[] bytes) {
		StringBuilder sbTmp = new StringBuilder(3 * bytes.length);

		for (byte b : bytes) {
			sbTmp.append(String.format("0x%02X", b));
			sbTmp.append(" ");
		}

		return sbTmp.toString();
	}

	public byte getCla() {
		return cla;
	}

	public void setCla(byte cla) {
		this.cla = cla;
	}

	public byte getIns() {
		return ins;
	}

	public void setIns(byte ins) {
		this.ins = ins;
	}

	public byte getP1() {
		return p1;
	}

	public void setP1(byte p1) {
		this.p1 = p1;
	}

	public byte getP2() {
		return p2;
	}

	public void setP2(byte p2) {
		this.p2 = p2;
	}

	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}

	public int getNe() {
		return ne;
	}

	public void setNe(int ne) {
		this.ne = ne;
	}

}
