/**
 * 2014
 * 15 juin 2014 16:12:18 
 * @author stephane stephane.gronowski@gmail.com
 */
package com.svan.javacard.cardUtils.request.common;


/**
 * 
 * 15 juin 2014 16:12:18
 * 
 * @author stephane stephane.gronowski@gmail.com
 * 
 */
public interface ReadRequest extends Request {

	public void setLength(Integer length);

	public void setOffset(Integer offset);
}
